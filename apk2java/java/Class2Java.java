import org.jd.core.v1.api.loader.Loader;
import org.jd.core.v1.api.loader.LoaderException;

import org.jd.core.v1.api.printer.Printer;

import org.jd.core.v1.ClassFileToJavaSourceDecompiler;
//import org.jd.core.v1.ClassFileToJavaSourceDecompiler.decompile;


import java.io.InputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.FileInputStream;

import java.nio.file.Files;
import java.nio.file.FileSystems;
import java.nio.file.FileSystem;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;
import java.io.File;
import java.awt.List;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import java.lang.Thread;

public class Class2Java
{
  public static ExecutorService executor =Executors.newSingleThreadExecutor();

  public static void listf(String directoryName) {
    File directory = new File(directoryName);

    // Get all files from a directory.
    File[] fList = directory.listFiles();
    if(fList != null)
      for (File file : fList) {
        if (file.isFile()) {
          //System.out.println(file.toString());
          if (file.toString().endsWith(".class"))
          {
            Callable<Void> task = new Callable<Void>() {
              public Void call() {
                decompile(file.toString());
                return null;
              }
            };
            Future<Void> future = executor.submit(task);
            try {
              future.get(1, TimeUnit.SECONDS);
            } catch (TimeoutException ex) {
              future.cancel(true);
              ex.printStackTrace();
              // handle the timeout
            } catch (InterruptedException e) {
              // handle the interrupts
            } catch (ExecutionException e) {
              // handle other exceptions
            } finally {
            }
          }
        } else if (file.isDirectory()) {
          listf(file.getAbsolutePath());
        }
      }
  }

  public static void decompile(String file)
  {
    Loader loader = new Loader() {
      @Override
      public byte[] load(String internalName) throws LoaderException {
        //InputStream is = this.getClass().getResourceAsStream("/" + internalName + ".class");
        //InputStream is = this.getClass().getResourceAsStream(internalName);
        try
        {
          final File initialFile = new File(internalName);
          InputStream is = new FileInputStream(initialFile);

          if (is == null) {
            return null;
          } else {
            try (InputStream in=is; ByteArrayOutputStream out=new ByteArrayOutputStream()) {
              byte[] buffer = new byte[1024];
              int read = in.read(buffer);

              while (read > 0) {
                out.write(buffer, 0, read);
                read = in.read(buffer);
              }

              return out.toByteArray();
            } catch (IOException e) {
              throw new LoaderException(e);
            }
          }
        }
        catch (Exception e)
        {
        }
        return null;

      }

      @Override
      public boolean canLoad(String internalName) {
        return this.getClass().getResource("/" + internalName + ".jar") != null;
      }
    };
    Printer printer = new Printer() {
      protected static final String TAB = "  ";
      protected static final String NEWLINE = "\n";

      protected int indentationCount = 0;
      protected StringBuilder sb = new StringBuilder();

      @Override public String toString() { return sb.toString(); }

      @Override public void start(int maxLineNumber, int majorVersion, int minorVersion) {}
      @Override public void end() {}

      @Override public void printText(String text) { sb.append(text); }
      @Override public void printNumericConstant(String constant) { sb.append(constant); }
      @Override public void printStringConstant(String constant, String ownerInternalName) { sb.append(constant); }
      @Override public void printKeyword(String keyword) { sb.append(keyword); }
      @Override public void printDeclaration(int type, String internalTypeName, String name, String descriptor) { sb.append(name); }
      @Override public void printReference(int type, String internalTypeName, String name, String descriptor, String ownerInternalName) { sb.append(name); }

      @Override public void indent() { this.indentationCount++; }
      @Override public void unindent() { this.indentationCount--; }

      @Override public void startLine(int lineNumber) { for (int i=0; i<indentationCount; i++) sb.append(TAB); }
      @Override public void endLine() { sb.append(NEWLINE); }
      @Override public void extraLine(int count) { while (count-- > 0) sb.append(NEWLINE); }

      @Override public void startMarker(int type) {}
      @Override public void endMarker(int type) {}
      };

    ClassFileToJavaSourceDecompiler decompiler = new ClassFileToJavaSourceDecompiler();

    //System.out.println(file);
    try
    {
      decompiler.decompile(loader, printer, file);
    }
    catch (Exception e)
    {
      System.out.println(e);
    }
    String source = printer.toString();
    System.out.println(source);

  }

  public static void main(String[] args) {
    //final FileSystem FS = FileSystems.getDefault();
    //Path modules = Paths.get("./lib");
    //listf("./lib");
    decompile(args[0]);

    //System.out.println("Test");
  }
}
